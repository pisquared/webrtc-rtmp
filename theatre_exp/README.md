# Theater Curious Bunch

## Install
```
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt
```

## Run

1. Run `img.py` and provide one of the COMMANDS (c/o/m/..). Right now it prints out the frames and uses PIL to generate images.


2. Listen on:
  - VLC:`vlc -> open -> Network stream`: `udp://0.0.0.0:1234`
  - ffplay: `ffplay udp://0.0.0.0:1234`
  - raw bytes (via netcat): `nc -ul | vlc -`

