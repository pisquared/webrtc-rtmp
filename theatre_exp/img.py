import os

from PIL import Image
from subprocess import Popen, PIPE

fps, duration = 30, 10


def create_ffmpeg_process():
    NOT_SPACE = "<<<>>>"

    command = """ffmpeg -re -stream_loop -1 -i - -vcodec mpeg4 -r 30 -vcodec mpeg4 -f mpegts udp://127.0.0.1:1234""".format(
        NOT_SPACE=NOT_SPACE)

    spl_command = [a.replace(NOT_SPACE, " ") for a in command.split()]

    p = Popen(spl_command, stdin=PIPE)
    return p


def finish_process(p):
    p.stdin.close()
    p.wait()


def color_animation(p):
    for i in range(fps * duration):
        print(i)
        im = Image.new("RGB", (800, 800), (i, 1, 1))
        im.save(p.stdin, 'PNG')


def open_image(p):
    for i in range(fps * duration):
        print(i)
        im = Image.open(os.path.join("assets", "bg.png"))
        im.save(p.stdin, "PNG")


def merge_images(p):
    bg_orig = Image.open(os.path.join("assets", "bg.png"))
    fg = Image.open(os.path.join("assets", "white.png"))
    for i in range(fps * duration):
        print(i)
        im = bg_orig.copy()
        im.paste(fg, (i % 100, i % 100), fg)
        im.save(p.stdin, "PNG")


COMMANDS = {
    "c": color_animation,
    "o": open_image,
    "m": merge_images,
}


def loop(p):
    # TODO: This print gets piped to process for some reason...
    # print("Commands:")
    # for key, command in COMMANDS:
    #     print("{}: {}".format(key, command.__name__))
    while True:
        c = input("> ")
        c = c.strip()
        if c in COMMANDS:
            COMMANDS[c](p)
        else:
            continue


def main():
    p = create_ffmpeg_process()
    loop(p)
    finish_process(p)


if __name__ == "__main__":
    main()
