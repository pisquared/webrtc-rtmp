import ffmpeg

background_file = ffmpeg
overlay_file = ffmpeg.input('./assets/transparent_bunch.png')
(
    ffmpeg
    .input('./assets/bg.png', framerate=30)
    .overlay(overlay_file)
    .output('udp://127.0.0.1:1234', format="mpegts", pix_fmt="yuv420p")
    .run()
)