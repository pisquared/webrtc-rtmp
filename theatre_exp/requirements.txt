asgiref==3.2.3
Django==3.0.1
ffmpeg-python==0.2.0
future==0.18.2
image==1.5.27
Pillow==6.2.1
pytz==2019.3
sqlparse==0.3.0
