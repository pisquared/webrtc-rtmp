#!/bin/bash
ffmpeg -re -stream_loop -1 -i ./assets/bg.png -i ./assets/temp.png \
-filter_complex "[0:v][1:v] overlay=25:25" \
-r 30 -vcodec mpeg4 \
-f mpegts \
udp://127.0.0.1:1234
