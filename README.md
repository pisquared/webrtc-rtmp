# WebRTC to RTMP

This provides a web interface to get your cam/mic and stream it to RTMP server.

It works through `WebRTC -> socket.io -> ffmpeg -> nginx+rtmp`

## TODO:
* [Auth](https://smartshitter.com/musings/2018/06/nginx-rtmp-streaming-with-slightly-improved-authentication/)
* [Re-publish to multiple sites](https://www.iamjack.co.uk/blog/post/setting-multistream-server-nginx)
* [Nginx RTMP directives](https://github.com/arut/nginx-rtmp-module/wiki/Directives)

## Resources:
* [GetUserMedia](https://www.html5rocks.com/en/tutorials/getusermedia/intro/)
* [PeerConnection](https://webrtc.github.io/samples/src/content/peerconnection/pc1/)
* [FFMPEG to RTMP](https://trac.ffmpeg.org/wiki/StreamingGuide)

## KEY WORDS:
* WebRTC
* RTC Peerconnection
* RTP to FFMPEG
* SDP
* FFMPEG to RTMP
