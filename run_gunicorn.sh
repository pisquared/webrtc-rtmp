#!/usr/bin/env bash
source venv/bin/activate
gunicorn -k geventwebsocket.gunicorn.workers.GeventWebSocketWorker -w 1 --bind localhost:5050 server:app