#!/bin/bash

set -x
set -e

echo "====================="
echo "Install requirements"
sudo apt update
sudo apt install -y unzip gcc make libpcre3-dev libssl-dev zlib1g-dev python-virtualenv ffmpeg software-properties-common

echo "====================="
echo "Download nginx and nginx-rtmp-module"
wget http://nginx.org/download/nginx-1.15.1.tar.gz
wget https://github.com/sergey-dryabzhinsky/nginx-rtmp-module/archive/dev.zip
tar -zxvf nginx-1.15.1.tar.gz
unzip dev.zip
cd nginx-1.15.1

echo "====================="
echo "configure/make/make install nginx with the rtmp module"
sudo useradd nginx-user
sudo mkdir -p /var/logs/nginx
./configure --with-http_ssl_module --with-http_v2_module  --add-module=../nginx-rtmp-module-dev
make
sudo make install
cd ..

echo "====================="
echo "Writing config to nginx.conf"
sudo cp rtmp.conf /usr/local/nginx/conf/nginx.conf

echo "====================="
echo "Setup python virtualenv and install requirements"
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt

echo "====================="
echo "install certbot"
sudo add-apt-repository universe
sudo add-apt-repository --yes ppa:certbot/certbot
sudo apt-get update
sudo apt-get install -y certbot python-certbot-nginx

echo "====================="
echo "Provision certificate"
sudo certbot --authenticator standalone --installer nginx --pre-hook "service nginx stop" --post-hook "service nginx start" --redirect --agree-tos --no-eff-email --email danieltcv@gmail.com -d video.pisquared.xyz --no-bootstrap

sudo mkdir -p /usr/local/nginx/conf/sites
sudo cp nginx_client_srv.conf /usr/local/nginx/conf/sites/nginx_client_srv.conf

echo "====================="
echo "Enabling gunicorn service"
sudo cp gunicorn.service /etc/systemd/system/gunicorn.service
sudo systemctl enable gunicorn.service
sudo systemctl daemon-reload
sudo systemctl start gunicorn.service

echo "====================="
echo "Starting nginx server"
sudo systemctl stop nginx
sudo /usr/local/nginx/sbin/nginx

echo "Done"
