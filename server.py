import argparse
import logging
from datetime import datetime
import subprocess
from threading import Thread
from time import sleep

from flask import Flask, render_template, request
from flask_socketio import SocketIO, emit

logging.basicConfig()
logger = logging.getLogger("webrtc-rtmp")

LOGLEVELS = {
    "error": logging.ERROR,
    "info": logging.INFO,
    "debug": logging.DEBUG,
}


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--loglevel', dest='loglevel', default="error")
    args = parser.parse_args()
    return args


def setup_logging_level(loglevel="error"):
    log_level = LOGLEVELS.get(loglevel.lower().strip(), logging.ERROR)
    logger.setLevel(log_level)
    logger.info("info enabled")
    logger.debug("debug enabled")


app = Flask(__name__)
app.config['SECRET_KEY'] = 'secrfdsffdsa89283098et!'
socketio = SocketIO(app,
                    # logger=True,
                    # engineio_logger=True,
                    cors_allowed_origins=[
                        "https://video.pisquared.xyz",
                        "http://localhost:5050"]
                    )

PROCESSES = {}
TIMEOUT = 30

ffmpeg_ops = [
    "ffmpeg",
    '-i', '-',
    '-c:v', 'libx264', '-preset', 'veryfast', '-tune', 'zerolatency',
    # video codec config: low latency, adaptive bitrate
    # '-c:a', 'aac', '-ar', '44100', '-b:a', '64k',
    # audio codec config: sampling frequency (11025, 22050, 44100), bitrate 64 kbits
    # '-y',  # force to overwrite
    # '-use_wallclock_as_timestamps', '1',  # used for audio sync
    # '-async', '1',  # used for audio sync
    # '-filter_complex', 'aresample=44100', # resample audio to 44100Hz, needed if input is not 44100
    # '-strict', 'experimental',
    '-bufsize', '10000',
    # '-f', 'flv',
    '-f', 'mpegts',
]


def start_ffmpeg_process(uuid):
    logger.info('process: start {}'.format(uuid))
    ffmpeg_args = ffmpeg_ops + ['udp://127.0.0.1:1234/']
    # ffmpeg_args = ffmpeg_ops + ['rtmp://localhost:1935/live/{}'.format(uuid)]
    p = subprocess.Popen(ffmpeg_args, stdin=subprocess.PIPE)
    logger.info('process: started {}'.format(uuid))
    return p


def kill_process(uuid):
    p = PROCESSES.get(uuid, {}).get("process")
    if p:
        logger.info('process: killing {}'.format(uuid))
        p.kill()
        logger.info('process: killed {}'.format(uuid))


def remove_process(uuid):
    if uuid in PROCESSES:
        del PROCESSES[uuid]


def get_now():
    return datetime.utcnow()


def gc():
    logger.info("gc: start")
    while True:
        now = get_now()
        logger.info("gc: wake up {}".format(now))
        remove = []
        logger.info("gc: iterating through {} processes".format(len(PROCESSES)))
        for uuid in PROCESSES.keys():
            ts = PROCESSES[uuid]["ts"]
            if (ts - now).seconds > TIMEOUT:
                logger.info("gc: mark uuid {} for killing - last active: {}".format(uuid, now))
                kill_process(uuid)
                remove.append(uuid)
        for k in remove:
            remove_process(k)
        logger.info("gc: sleeping {}".format(TIMEOUT))
        sleep(TIMEOUT)


@socketio.on('start')
def handle_connect():
    uuid = str(request.sid)
    logger.info("connect: session id: {}".format(uuid))
    PROCESSES[uuid] = ({
        "uuid": uuid,
        "process": start_ffmpeg_process(uuid),
        "ts": get_now()
    })
    emit('connected', {
        "uuid": uuid,
    })


@socketio.on('disconnect')
def handle_disconnect():
    uuid = str(request.sid)
    logger.info('disconnect: stop {}'.format(uuid))
    kill_process(uuid)
    remove_process(uuid)


@socketio.on('binarystream')
def handle_start(message):
    uuid = message.get('uuid')
    data = message.get('data')
    if uuid is not None and data is not None:
        logger.debug("binary: got {} B data for {}".format(len(data), uuid))
        p = PROCESSES.get(uuid, {}).get("process")
        if not p:
            logger.info("binary: no process {}".format(uuid))
            emit("fatal", "no process {}".format(uuid))
            return
        try:
            p.stdin.write(data)
            PROCESSES[uuid]["ts"] = get_now()
        except Exception as e:
            logging.exception("binary: Error for {}".format(uuid))
            emit("fatal", str(e))
            kill_process(uuid)
            remove_process(uuid)


@app.route('/')
def home():
    return render_template("home.html")


@app.route('/canvas')
def canvas():
    return render_template("canvas.html")


@app.route('/com')
def com():
    return render_template("com.html")


@app.route('/camtracker')
def camtracker():
    return render_template("camtracker.html")


# t = Thread(target=gc)
# t.start()


def main(args):
    socketio.run(app, port=5050)
    # t.join()


if __name__ == "__main__":
    args = parse_args()
    setup_logging_level(args.loglevel)
    main(args)
