#!/usr/bin/env bash
gcloud beta compute --project=pisquared-support-4 instances create rtmp \
  --zone=europe-west6-a \
  --machine-type=n1-standard-2 \
  --subnet=default \
  --address=34.65.219.210 \
  --network-tier=PREMIUM \
  --tags=rtmp,http-server,https-server \
  --image=ubuntu-1804-bionic-v20191008 \
  --image-project=ubuntu-os-cloud \
  --boot-disk-size=100GB \
  --boot-disk-type=pd-standard \
  --boot-disk-device-name=rtmp \
  --reservation-affinity=any